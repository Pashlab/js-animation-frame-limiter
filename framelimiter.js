  var APP = {
    tick: function () {
      // do an animation frame
    }
  };

  APP.skip = 1; // how many frames to skip
  var S = 0;
  APP.anim = function() {
    S += 1;
    if (S >= APP.skip) {
      S = 0;
      APP.tick();
    }
    requestAnimationFrame(APP.anim);
  }
